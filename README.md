## Prerequisite

- **Docker** - https://docs.docker.com/install/linux/docker-ce/ubuntu/
- **Docker Compose** - https://docs.docker.com/compose/install/

## Steps for run

1. git clone git@bitbucket.org:paulocesarmsf/sorting-service.git
2. docker-compose up (at root the project)

## Steps for execute
You can access http://localhost:5000/apidocs/#!/default/post_sorting_books and visualize and interact with API resources.

or

Make a post for route

- method: POST
- route: http://localhost:5000/sorting_books
- body:

```javascript
    { 
      "books": [
        {
          "author": "String",
          "edition_year": Integer,
          "title": "String"
        },
        {
          "author": "String",
          "edition_year": Integer,
          "title": "String"
        }
      ],
      "descending": boolean,
      "filter": "String"
    }
```

## Example

```javascript
    { 
      "books": [
        {
          "author": "JK. Rowling",
          "edition_year": 1997,
          "title": "Harry Potter The Philosopher's Stone"
        },
        {
          "author": "JK. Rowling",
          "edition_year": 1998,
          "title": "Harry Potter The Chamber of Secrets"
        }
      ],
      "descending": true,
      "filter": "title"
    }
```
