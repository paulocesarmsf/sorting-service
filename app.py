from flask import Flask, request, jsonify
from flasgger import Swagger, swag_from

from exceptions.error_not_found import BooksNotFound
from workers.sorter import Sorter


app = Flask(__name__)

swagger = Swagger(app)
sorter = Sorter()


@app.route('/spec', methods=['GET'])
def index():
    return 'ok'


@swag_from('schemas/sorting_books.yml')
@app.route('/sorting_books', methods=['POST'])
def sorting_books():
    try:
        return jsonify(data=sorter.sorting(request.get_json()))
    except BooksNotFound as b:
        return jsonify(msg_error=b.message), b.status_code
    except Exception:
        return jsonify(msg_error='Error sorting your books'), 500


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
