import logging


class BooksNotFound(Exception):
    def __init__(self):
        self.message = 'books not found'
        self.status_code = 400
        logging.error(self.message)
