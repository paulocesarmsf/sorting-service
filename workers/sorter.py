from exceptions.error_not_found import BooksNotFound


class Sorter(object):

    def sorting(self, incoming_data):
        self.validate_incoming_data(incoming_data)
        books = incoming_data.get('books')
        field_filter = incoming_data.get('filter')
        if books and field_filter:
            return sorted(books,
                          key=lambda x: x[field_filter],
                          reverse=incoming_data.get('descending', False))
        return books

    def validate_incoming_data(self, incoming_data):
        if 'books' not in list(incoming_data.keys()):
            raise BooksNotFound()
