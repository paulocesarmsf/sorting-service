import unittest
from workers.sorter import Sorter


class SorterTest(unittest.TestCase):

    incoming_data = {
                     "filter": "title",
                     "descending": False,
                     "books": [{"title": "F", "author": "7", "edition_year": 2007},
                               {"title": "D", "author": "2", "edition_year": 2002},
                               {"title": "C", "author": "3", "edition_year": 2003},
                               {"title": "B", "author": "1", "edition_year": 2001},
                               {"title": "A", "author": "4", "edition_year": 2004},
                               {"title": "A", "author": "5", "edition_year": 2005},
                               {"title": "A", "author": "6", "edition_year": 2006}]
                    }

    def test_sorter_ascending(self):
        books = Sorter().sorting(self.incoming_data)
        expected_data = [{'title': 'A', 'author': '4', 'edition_year': 2004},
                        {'title': 'A', 'author': '5', 'edition_year': 2005},
                        {'title': 'A', 'author': '6', 'edition_year': 2006},
                        {'title': 'B', 'author': '1', 'edition_year': 2001},
                        {'title': 'C', 'author': '3', 'edition_year': 2003},
                        {'title': 'D', 'author': '2', 'edition_year': 2002},
                        {'title': 'F', 'author': '7', 'edition_year': 2007}]
        self.assertEqual(books, expected_data)

    def test_sorter_descending(self):
        books = Sorter().sorting(self.incoming_data)
        expected_data = [{"author": "4", "edition_year": 2004, "title": "A"},
                        {"author": "5", "edition_year": 2005, "title": "A"},
                        {"author": "6", "edition_year": 2006, "title": "A"},
                        {"author": "1", "edition_year": 2001, "title": "B"},
                        {"author": "3", "edition_year": 2003, "title": "C" },
                        {"author": "2", "edition_year": 2002, "title": "D"},
                        {"author": "7", "edition_year": 2007, "title": "F"}]
        self.assertEqual(books, expected_data)

    def test_sorter_empty_books(self):
        incoming_data = {
            "filter": "title",
            "descending": False,
            "books": []
        }
        books = Sorter().sorting(incoming_data)
        self.assertEqual(books, [])

    def test_sorter_dont_filter(self):
        incoming_data = {
                     "books": [{"title": "F", "author": "7", "edition_year": 2007},
                               {"title": "D", "author": "2", "edition_year": 2002},
                               {"title": "C", "author": "3", "edition_year": 2003},
                               {"title": "B", "author": "1", "edition_year": 2001},
                               {"title": "A", "author": "4", "edition_year": 2004},
                               {"title": "A", "author": "5", "edition_year": 2005},
                               {"title": "A", "author": "6", "edition_year": 2006}]
                    }
        expected_data = incoming_data.get('books')
        books = Sorter().sorting(incoming_data)
        self.assertEqual(books, expected_data)

    def test_sorter_author(self):
        incoming_data = {
            "filter": "author",
            "descending": False,
            "books": [{"title": "F", "author": "7", "edition_year": 2007},
                      {"title": "D", "author": "2", "edition_year": 2002},
                      {"title": "C", "author": "3", "edition_year": 2003},
                      {"title": "B", "author": "1", "edition_year": 2001},
                      {"title": "A", "author": "4", "edition_year": 2004},
                      {"title": "A", "author": "5", "edition_year": 2005},
                      {"title": "A", "author": "6", "edition_year": 2006}]
        }
        books = Sorter().sorting(incoming_data)
        expected_data = [{'title': 'B', 'author': '1', 'edition_year': 2001},
                         {'title': 'D', 'author': '2', 'edition_year': 2002},
                         {'title': 'C', 'author': '3', 'edition_year': 2003},
                         {'title': 'A', 'author': '4', 'edition_year': 2004},
                         {'title': 'A', 'author': '5', 'edition_year': 2005},
                         {'title': 'A', 'author': '6', 'edition_year': 2006},
                         {'title': 'F', 'author': '7', 'edition_year': 2007}]
        self.assertEqual(books, expected_data)

    def test_sorter_author(self):
        incoming_data = {
            "filter": "edition_year",
            "descending": False,
            "books": [{"title": "F", "author": "7", "edition_year": 2007},
                      {"title": "D", "author": "2", "edition_year": 2002},
                      {"title": "C", "author": "3", "edition_year": 2003},
                      {"title": "B", "author": "1", "edition_year": 2001},
                      {"title": "A", "author": "4", "edition_year": 2004},
                      {"title": "A", "author": "5", "edition_year": 2005},
                      {"title": "A", "author": "6", "edition_year": 2006}]
        }
        books = Sorter().sorting(incoming_data)
        expected_data = [{'title': 'B', 'author': '1', 'edition_year': 2001},
                         {'title': 'D', 'author': '2', 'edition_year': 2002},
                         {'title': 'C', 'author': '3', 'edition_year': 2003},
                         {'title': 'A', 'author': '4', 'edition_year': 2004},
                         {'title': 'A', 'author': '5', 'edition_year': 2005},
                         {'title': 'A', 'author': '6', 'edition_year': 2006},
                         {'title': 'F', 'author': '7', 'edition_year': 2007}]
        self.assertEqual(books, expected_data)